#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import os
import neat
import pickle
import ta
from collections import deque
import random
import time
from statistics import mean

#hide warnings
import warnings
warnings.filterwarnings('ignore')


# In[2]:


sequence_length = 30
trading_days = 400


# In[3]:


# #START HERE
# for stock,data in tradable_stock_df:
#     print(stock,len(data))
def run(config_file):
    #globals
    overall_profits = 0
    
    
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         config_file)

    pe = neat.parallel.ParallelEvaluator(8, eval_genomes)
    #loading winner net
#     winner = pickle.load(open("winner_net7.pickle","rb"))
    
    #creating the winner net
#     winner_net = neat.nn.RecurrentNetwork.create(winner, config)

    
    # # Create the population, which is the top-level object for a NEAT run.
    p = neat.Population(config)

            #continue from save point
    # p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-2')
    
    # Add a stdout reporter to show progress in the terminal.
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(5))
    
    # Run for up to 300 generations.
    winner = p.run(pe.evaluate,30)
    save_winner(winner)

    # Display the winning genome.
    print('\nBest genome:\n{!s}'.format(winner))


# In[4]:



def eval_genomes(genome, config):
    global stock_data

    genome.fitness = 0.0
    net = neat.nn.RecurrentNetwork.create(genome, config)
    
    #over all profits
    list_profits = []
    overall_profits = 0
        
    stock_list = [stock for stock,data in stock_data]
#         print(stock_list)
    random.shuffle(stock_list)
#         print(stock_list)
    
    
    #looping through all the stocks
    for stock in stock_list:
        data = stock_data.get_group(stock)
        if len(data) > 30:
            # print(stock,len(data))

            data = process_data(data)

#             #results based on how many trading days
#             if len(data) > trading_days:
#                 data = data[-trading_days:]
            # data = data.loc['2018-11-05':]
            data = sampler(data)


            # print(f"trading days:{len(data)} start:{data.index[0]}")


            stock_profit = get_action(data,net)
#             print(data.tail(10))
            list_profits.append(stock_profit)               
            overall_profits +=stock_profit
    
    fitness = mean(list_profits)
    genome.fitness = fitness
    
    print(f"fitness:{fitness}")
    print(f"overall profits: {overall_profits} %")

    return genome.fitness


# In[5]:




    

    
def save_winner(winner):

    pickle_out = open("winner_net.pickle", "wb")
    pickle.dump(winner, pickle_out)
    pickle_out.close()

# In[6]:


def get_action(data,net):
    #1stock
    monthly = 30
    yearly = 300
    
    #initialize deque sequence
    sequence = deque_sequence(data)

    """ 6(sequence) x 5(open,high,low,close)
            0, 0, 0, 0, 0
            0, 0, 0, 0, 0
            0, 0, 0, 0, 0
            0, 0, 0, 0, 0
            0, 0, 0, 0, 0 """
    
    #POSITIONS
    position_change = 0
    position_days = 0
    
    trades = 0
    wins = 0
    loss = 0
    
    stock_profits = 0
    
    day_counter = 0
    monthly_profits = 0
    yearly_profits = 0
    
    
    #FEEDING THE NEURAL NET
    for vals in data.values[:]:
        #append the values of data (open,high,low,close) to deque sequence
        sequence.append(vals)
        
        #convert deque function to a numpy array
        x = np.array(sequence)

        #flatten features
        x = x.flatten()   

        #append positon_change and position days ... more feature
        x = np.append(x,[position_change,position_days])
        
        #feed features to neural network
        output = net.activate(x)

        #action recomended by the neural network 
        action = np.argmax(output, axis=0)
        
#         print(action)
        profit,wins,loss,trades,position_days,position_change = do_action(vals,action,position_days,position_change,trades,wins,loss)
        
        stock_profits += profit
        monthly_profits += profit
        yearly_profits += profit
        
        day_counter += 1
        
        #print monthly profit
        # if (day_counter%monthly) == 0:
        #     print(f"monthly profit:{monthly_profits} %")
        #     monthly_profits = 0
#             print(day_counter,monthly)
            
        #print yearly profit
#         if (day_counter%yearly) == 0:
#             print("")
#             print(f"yearly profit:{yearly_profits} %")
# #             print(day_counter,yearly)
#             print("")
#             yearly_profits = 0
            
            
                
        
    print("")
    print(f"overall_stock profit:{stock_profits} %")
    print(f"stock trades:{trades}")
    print(f"stock wins:{wins}")
    print(f"stock loss:{loss}")
    print("")
    
    return stock_profits


# In[7]:


def do_action(vals,action,position_days,position_change,trades,wins,loss):
    profit = 0
    
#     """if action is BUY and has no position"""
    if action == 1 and position_days == 0 :
        position_days = 1
        position_change = -1.19/100
    
#     """if action is BUY and has position"""
    elif action == 1 and position_days > 0 :
        position_days += 1
        position_change += vals[0] #vals[0] = data['close']
        
        #SELL if position is 60 days older or -10%
        if position_days >= 60 or position_change < -10/100:
            #SELL
            trades += 1
            
            #check trade if win or loss
            if position_change > 0 :
                wins += 1
                profit = position_change*100
#                 print(f"profit:{position_change*100} days:{position_days}")
                
            else:
                loss += 1
                profit = position_change*100
#                 print(f"profit:{position_change*100} days:{position_days}")
                
            
            #RESET
            position_days = 0
            position_change = 0
            
#     """if action is SELL and has no position"""
    elif action == 2 and position_days == 0 :
        pass
    
#     """if action is SELL and has position"""
    elif action == 2 and position_days > 0 :
        position_days += 1
        position_change += vals[0]
        trades += 1
        
        if position_change > 0:
            wins += 1
            profit = position_change*100
#             print(f"profit:{position_change*100} days:{position_days}")
            
        else:
            loss += 1
            profit = position_change*100
#             print(f"profit:{position_change*100} days:{position_days}")
            
        
        #RESET
        position_days = 0
        position_change = 0
        
#     """if action is hold and has no position"""
    elif action == 0 and position_days == 0 :
        pass
    
#     """if action is hold and has position"""
    elif action == 0 and position_days > 0 :
        position_days += 1
        position_change += vals[0]
        
        #sell if position is 60 days older
        if position_days >= 60 or position_change < -10/100:
            #SELL
            trades += 1
            
            #check trade if win or loss
            if position_change > 0 :
                wins += 1
                profit = position_change*100
#                 print(f"profit:{position_change*100} days:{position_days}")
            else:
                loss += 1
                profit = position_change*100
#                 print(f"profit:{position_change*100} days:{position_days}")
            
            #RESET
            position_days = 0
            position_change = 0
        
        
            
    return profit,wins,loss,trades,position_days,position_change


# In[8]:


#initialize deque sequence
def deque_sequence(data):
    global sequence_length
    #initialize deque .. sequence of
    #number of sequence by open ,high ,low ,close++
    sequence = deque(maxlen=sequence_length)
    
    #if sequence_length = 6 .. it looks like this.. a 6 by 5 matrix
    for _ in range(sequence_length):
        sequence.append([0 for _ in data.columns])
        
        """ 6(sequence) x 5(open,high,low,close)
                        0, 0, 0, 0, 0
                        0, 0, 0, 0, 0
                        0, 0, 0, 0, 0
                        0, 0, 0, 0, 0
                        0, 0, 0, 0, 0 """
    return sequence


# In[9]:


def process_data(data):
    data.drop("Netforeign",1,inplace=True) #drop netforeign
    data.drop("stock code",1,inplace=True) #drop stock code
    data['Volume'].replace(0,1,inplace=True) #replace 0 value volume with 1
    data.interpolate(inplace=True)
    
    #adding technical indicators
    data = ta.add_all_ta_features(data,
                              open="Open",
                              high="High",
                              low="Low",
                              close="Close",
                              volume="Volume",
                              fillna=True)
    
    col_list = ["Open","High","Low","Close","Volume",
            "momentum_rsi","momentum_wr","momentum_ao","momentum_stoch","momentum_stoch_signal",
            "trend_trix","trend_ema_slow","trend_ema_fast","trend_vortex_ind_pos","trend_vortex_ind_neg",
            "trend_vortex_diff","trend_macd","trend_macd_signal","trend_macd_diff",
           "volatility_atr"]
    
    data = data[col_list]
    
    #OPEN , HIGH, LOW, CLOSE, VOLUME
    data['Open'] = data['Open'].pct_change()
    data['High'] = data['High'].pct_change()
    data['Low'] = data['Low'].pct_change()
    data['Close'] = data['Close'].pct_change()
    data['Volume'] = data['Volume'].pct_change()
    data['trend_ema_slow'] = data['trend_ema_slow'].pct_change()
    data['trend_ema_fast'] = data['trend_ema_fast'].pct_change()
    data['momentum_rsi'] = data['momentum_rsi']/100
    data['momentum_wr'] = data['momentum_wr']/100
    data['momentum_stoch'] = data['momentum_stoch']/100
    data['momentum_stoch_signal'] = data['momentum_stoch_signal']/100
    
    data.dropna(inplace=True)
    
    return data
    


# In[10]:


#reading all csv from data folder and combine them all
def load_data():

    dfs = []

    for item in os.listdir('data'):
        df = pd.read_csv(f'data/{item}',
                        header=None,
                        names=['stock code','Date','Open','High','Low','Close','Volume','Netforeign'])
        df['Date'] = pd.to_datetime(df['Date'])
        df.dropna(inplace=True)
        df.set_index('Date',inplace=True)

        #sort values by date
        df.sort_values('Date',inplace=True)
        dfs.append(df)

    main_df = pd.concat(dfs)
    main_df.tail()
    
    ##################################################################################################
    #read tradeble stocks
    tradable = pd.read_csv('tradable.csv')
    
    #creating a new df of tradable stock
    tradable_stock_df = main_df[main_df['stock code'].isin(tradable['stock'])]
    tradable_stock_df.head()
    
    tradable_stock_list = tradable_stock_df['stock code'].unique()
    tradable_stock_list.sort()

    # print(tradable_stock_list,len(tradable_stock_list))
    # print("\n\n")
    
    #group by tradable stock
    tradable_stock_df=tradable_stock_df.groupby('stock code')
    
    return tradable_stock_df

def sampler(data):
    sample = False
    data = data
    data_copy = data
    while sample == False:
        start = random.randint(0,len(data_copy)-1)
        end = random.randint(start,len(data_copy)-1)
        data = data_copy.iloc[start:end]
        if len(data)>30:
            sample = True
    print(data.index[0],data.index[-1])
    return data
    


# In[11]:
#loading the combine csv
stock_data = load_data()

if __name__ == '__main__':
    # Determine path to configuration file. This path manipulation is
    # here so that the script will run successfully regardless of the
    # current working directory.
    local_dir = os.path.dirname('__file__')
    config_path = os.path.join(local_dir, 'config-feedforward.txt')
    run(config_path)


# In[ ]:




